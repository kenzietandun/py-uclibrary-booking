#!/usr/bin/env python3

import argparse
from datetime import datetime
from datetime import timedelta
import configparser
from client import Client


def add_program_args(p):
    current_date = datetime.today()
    current_date = current_date.strftime("%d%m%y")

    p.add_argument("-d",
                   "--date",
                   help="set the date of booking in DDMMYY",
                   default=current_date)

    p.add_argument("-t",
                   "--to",
                   help="set the range of date to book in DDMMYY",
                   default=current_date)

    p.add_argument(
        "-f",
        "--friends",
        help=
        "set your booking partner separated by comma e.g. user1,user2,user3",
        default="")

    p.add_argument("-du",
                   "--duration",
                   help="set the duration of your booking, defaults to 2",
                   choices=["1", "2"],
                   default="2")

    p.add_argument("-s",
                   "--size",
                   help="set the minimum number of people that a room can fit",
                   default=5,
                   type=int)

    p.add_argument(
        "--time",
        help="set the time constraint for booking, defaults to 08:00-22:00",
        default="08:00-22:00")


def main():
    config = configparser.ConfigParser()
    config.read("config.ini")
    username = config["credentials"]["username"]
    password = config["credentials"]["password"]

    parser = argparse.ArgumentParser()
    add_program_args(parser)
    args = parser.parse_args()

    time_constraint = args.time.split("-")

    c = Client(username, password)
    c.friends = [x for x in args.friends.split(",") if x]
    c.min_size = args.size
    c.time_constraint = args.time.split("-")

    start_date = datetime.strptime(args.date, "%d%m%y")
    end_date = datetime.strptime(args.to, "%d%m%y")
    num_days = (end_date - start_date).days
    dates = [start_date + timedelta(days=x) for x in range(0, num_days + 1)]

    if not dates:    # only book the current day
        dates = [start_date]

    for date in dates:
        book_day = date.strftime("%d")
        book_month = date.strftime("%m")
        book_year = date.strftime("%y")

        c.day = book_day
        c.month = book_month
        c.year = book_year

        c.find_largest_room()
        c.book_room(args.duration)


if __name__ == "__main__":
    main()
