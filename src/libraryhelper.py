#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup as bs

# hardcoded level URLs
LEVEL_2 = "54"
LEVEL_8 = "60"
LEVEL_9 = "2"
LEVEL_11 = "3"

ALL_LEVELS = {2: LEVEL_2, 8: LEVEL_8, 9: LEVEL_9, 11: LEVEL_11}

START_TIME = "12:00"
END_TIME = "18:00"

root_url = "http://library.canterbury.ac.nz/webapps/mrbs/day.php?page=1&year={year}&month={month}&day={day}&area={level}"


# Goes to the url specified and returns a soup object that
# can be used by other functions to parse the web info
def get_webpage(date_info, level):
    year, month, day = date_info
    data = requests.get(
        root_url.format(year=year, month=month, day=day, level=level))
    soup = bs(data.text, "html.parser")
    return soup


# Extracts the UC username from a booking description
def extract_username_from_booking_desc(booking_desc):
    return booking_desc.split(' ')[-1][1:-1]


# Extracts the table from the booking webpage
# Returns List of bookings, False means it is available
def extract_booking_schedule(booking_soup):
    schedule_table = booking_soup.find("table", width="100%", border="1")
    counter = 0
    is_time_column = 1
    column_num = 0

    booking_schedule = []
    for tr in schedule_table.find_all("tr"):    # for each booking row
        if counter == 0:
            counter += 1
            continue

        is_time_column = 1
        booking_row = []
        column_num = 0
        for td in tr.find_all("td"):
            if is_time_column:
                booking_row.append(td.text)
                is_time_column = 0
            else:
                booked = td["class"][0] == "I"
                if booked:
                    booked_from_previous_hour = td.text == ' " '
                    if booked_from_previous_hour:
                        booking_row.append(booking_schedule[-1][column_num])
                    else:
                        booking_row.append(
                            extract_username_from_booking_desc(td.text))
                else:
                    booking_row.append(booked)

            column_num += 1
        booking_schedule.append(booking_row)
    return [*zip(*booking_schedule)]


# Extracts the room size from a room's name
def extract_room_size_from_name(room_name):
    room_name_split_space = room_name.split(' ')
    room_size_info = room_name_split_space[-1]
    return room_size_info[1:-1]


# Extracts all the rooms inside a level
# date_info should be a tuple with year, month and day
def get_rooms_info_from_level(soup):
    rooms = []
    for room in soup.find_all("td", align="center", valign="middle"):
        rooms.append(room.text)

    rooms = ["Time"] + rooms
    return rooms


def set_date_info(year, month, day):
    return (year, month, day)


def calculate_time_constraint_index(time):
    STARTING_TIME = 8
    HALF_HOUR = 2

    hour, minute = time.split(":")
    hour = int(hour)
    minute = int(minute)

    index = 0
    index += (hour - STARTING_TIME) * HALF_HOUR
    if minute == 30:
        index += 1

    return index


def get_room_id(level, room_index):
    starting_index = 0

    if level == 2:
        starting_index = 214
        if room_index <= 3:
            return starting_index + room_index
        if room_index >= 6:
            return starting_index + room_index - 2
        else:
            return starting_index + room_index + 7
    elif level == 8:
        starting_index = 241
        return starting_index + room_index
    elif level == 9:
        starting_index = -1
        if room_index == 1:
            return 152
        elif room_index >= 2 and room_index <= 5:
            return starting_index + room_index
        else:
            return 15
    elif level == 11:
        starting_index = 4
        if room_index <= 4:
            return starting_index + room_index
        else:
            starting_index = 12
            return starting_index + room_index


# Time constraint is a tuple of (str, str) indicating start and end time
def calculate_room_score(room_schedule, time_constraint):
    longest_interval = 0
    current_interval = 0
    starting_book_time = 0
    curr_start_time = 0

    start_time, end_time = time_constraint

    start_time_index = calculate_time_constraint_index(start_time)
    end_time_index = calculate_time_constraint_index(end_time)

    for i in range(len(room_schedule)):
        time_interval = room_schedule[i]
        if i < start_time_index or i >= end_time_index:
            continue

        if not time_interval:    # if time interval is not booked
            if current_interval == 0:
                curr_start_time = i

            current_interval += 1
        else:
            current_interval = 0

        if current_interval > longest_interval:
            longest_interval = current_interval
            starting_book_time = curr_start_time

    return (starting_book_time, longest_interval)


def find_friends_booking(room_schedule, friends):
    book_after_this = 0
    starting_book_time_index = 0

    for i in range(len(room_schedule)):
        time_interval = room_schedule[i]
        for friend in friends:
            if time_interval and friend in time_interval:    # room is booked by someone in our friends list
                book_after_this = 1
                break

        if not time_interval and book_after_this:    # room is not booked
            starting_book_time_index = i
            break

    return starting_book_time_index


def find_largest_empty_rooms(date_info, min_room_size, time_constraint,
                             friends):
    starting_book_time = 0

    rooms = []

    for level, level_url in ALL_LEVELS.items():
        soup = get_webpage(date_info, level_url)
        rooms_info = get_rooms_info_from_level(soup)
        level_schedule = extract_booking_schedule(soup)
        for i in range(1, len(rooms_info)):
            room_name = rooms_info[i]
            room_size = extract_room_size_from_name(room_name)
            room_score = 0

            if int(room_size) < min_room_size:
                continue

            room_schedule = level_schedule[i]
            # if we want to book with friends
            if friends:
                room_score = 100    # arbitarily large number to make sure its the highest score
                book_time = find_friends_booking(room_schedule, friends)
                if book_time == 0:    # our friends are not booked in this room
                    continue    # go to the next room
            else:
                book_time, room_score = calculate_room_score(
                    room_schedule, time_constraint)

            starting_book_time = level_schedule[0][book_time]
            room_number = get_room_id(level, i)

            rooms.append((level, room_name, ALL_LEVELS[level], room_number,
                          starting_book_time, room_score))

    return sorted(rooms, key=lambda x: x[5], reverse=True)


def generate_booking_request_param(client, duration):
    params = {
        "name": "Booked with automated booking system",
        "description": "",
        "day": client.day,
        "month": client.month,
        "year": client.year,
        "hour": client.book_hour,
        "minute": client.book_minute,
        "duration": duration,
        "dur_units": "hours",
        "type": "I",
        "page": "1",
        "area": client.room_area,
        "returl": "",
        "room_id": client.room_number,
        "create_by": client.username,
        "rep_id": "0",
        "edit_type": ""
    }
    return params


def is_booking_successful(response):
    soup = bs(response.text, "html.parser")
    for h2 in soup.find_all("h2"):
        text = h2.text
        if text == "To make a booking":
            return True

    return False


# Main function
def main():
    pass


if __name__ == "__main__":
    main()
