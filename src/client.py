#!/usr/bin/env python3

import requests
import libraryhelper as helper
import time
from libraryhelper import ALL_LEVELS as levels_dict

book_url = "http://library.canterbury.ac.nz/webapps/mrbs/edit_entry_handler.php?"


class Client:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.day = ""
        self.month = ""
        self.year = ""
        self.friends = []
        self.min_size = 0
        self.time_constraint = ""
        self.rooms_info = []
        self.room_level = ""
        self.room_name = ""
        self.room_area = ""
        self.room_number = ""
        self.book_hour, self.book_minute = "", ""

    def find_largest_room(self):
        date_info = helper.set_date_info(self.year, self.month, self.day)
        self.rooms_info = helper.find_largest_empty_rooms(
            date_info, self.min_size, self.time_constraint, self.friends)

    def book_room(self, duration):
        session = requests.Session()
        session.auth = (self.username, self.password)
        for room_info in self.rooms_info:
            self.room_level = room_info[0]
            self.room_name = room_info[1]
            self.room_area = room_info[2]
            self.room_number = room_info[3]
            book_time = room_info[4]
            self.book_hour, self.book_minute = book_time.split(":")

            response = session.get(
                book_url,
                params=helper.generate_booking_request_param(self, duration))

            if helper.is_booking_successful(response):
                print("Booked room {} -- {} {} {}".format(
                    self.room_name, self.day, self.month, self.year))
                break
            time.sleep(2)


def main():
    pass


if __name__ == "__main__":
    main()
